<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new Illuminate\Foundation\Application;

/*
|--------------------------------------------------------------------------
| Detect The Application Environment
|--------------------------------------------------------------------------
|
| Laravel takes a dead simple approach to your application environments
| so you can just specify a machine name for the host that matches a
| given environment, then we will automatically detect it for you.
|
*/

$env = $app->detectEnvironment(function()
{

	if ( isset($_SERVER['HTTP_USER_AGENT']) ) {
		if ( $_SERVER['HTTP_USER_AGENT'] == 'Unit-Tests' ) {
			return 'testing';
		}
	}

	switch (true){
		case (gethostname() == 'Lenovo-Jonathan'): return 'dev';
		case ( stristr(__DIR__, '/Users/didier/Devhouse/Projects/Mobile_Guardian/Code/Mobile_Guardian') ): return 'testing';
		case ( stristr(__DIR__, '/Volumes/webvol/Documents/mobileguardian/mgshortie') ): return 'nisof';
		case ( stristr(__DIR__, '/Library/WebServer/Documents/MobileGuardianWorkspace/school-dashboard') ): return 'dev-al';
		case ( stristr(__DIR__, '/Users/didierbreedt/Devhouse/Projects/Mobile_Guardian/Code/school-dashboard') ): return 'testing';
		case ( getenv('MF_ENV') && substr(getenv('MF_ENV'),0,2) == 'ua' ) :  return 'uat';
		case ( getenv('MF_ENV') && substr(getenv('MF_ENV'),0,2) == 'st' ) :  return 'stg';
		case ( getenv('MF_ENV') && substr(getenv('MF_ENV'),0,2) == 'pr' ) :  return 'prd';
		case ( getenv('MF_ENV') && substr(getenv('MF_ENV'),0,3) == 'dev' ) :  return 'dev';
		case ( getenv('MF_ENV') && substr(getenv('MF_ENV'),0,2) == 'de' ) :  return 'dev';
		case ( getenv('MF_ENV') && substr(getenv('MF_ENV'),0,2) == 'al' ) :  return 'alpha';
		case ( stristr(__DIR__, '/home/alpha/mdm_laravel4/mdm') ): return 'alpha';
		case ( getenv('VN_ENV') && substr(getenv('VN_ENV'),0,2) == 'al' ) :  return 'alpha-virtuenet';
		case ( getenv('MF_ENV') && substr(getenv('MF_ENV'),0,6) == 'vn-prd' ) :  return 'vn-prd';
		case ( getenv('MF_ENV') && substr(getenv('MF_ENV'),0,2) == 'be' ) :  return 'beta';
		case ( stristr(__DIR__, 'production-testing') ): return 'production-testing';
		case ( stristr(__DIR__, 'production-staging')): return 'production-staging';
		default: return 'production-live';
	}
});


/*
|--------------------------------------------------------------------------
| Bind Paths
|--------------------------------------------------------------------------
|
| Here we are binding the paths configured in paths.php to the app. You
| should not be changing these here. If you need to change these you
| may do so within the paths.php file and they will be bound here.
|
*/

$app->bindInstallPaths(require __DIR__.'/paths.php');

/*
|--------------------------------------------------------------------------
| Load The Application
|--------------------------------------------------------------------------
|
| Here we will load this Illuminate application. We will keep this in a
| separate location so we can isolate the creation of an application
| from the actual running of the application with a given request.
|
*/

$framework = $app['path.base'].
                 '/vendor/laravel/framework/src';

require $framework.'/Illuminate/Foundation/start.php';

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
