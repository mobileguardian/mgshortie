<?php

return array(

    'log_level' => Log::DEBUG,
    'log_path'  => getenv('MF_LOG') ?: path('storage') . 'logs/',
    'log'       => true,

);
