<?php

return array(
	'safari_prd_enroll'   => 'https://mdm.mobileguardian.com/profile/enroll?sig=DEP&is_dep=0&is_mdm_enroll=1&instance={code}',
	'safari_uat_enroll'    => 'https://mdm.uat.mobileguardian.com/profile/enroll?sig=DEP&is_dep=0&is_mdm_enroll=1&instance={code}',
	'safari_stg_enroll'  => 'https://qa-mdm.mobileguardian.com/profile/enroll?sig=DEP&is_dep=0&is_mdm_enroll=1&instance={code}',
    'dep_enroll'            => '',
    'configurator_enroll'   => ''

);

