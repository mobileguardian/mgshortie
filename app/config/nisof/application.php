<?php

return array(
	'safari_prod_enroll'   => 'https://mdm.mobileguardian.com/profile/enroll?sig=DEP&is_dep=0&is_mdm_enroll=1&instance={code}',
	'safari_uat_enroll'    => 'https://mdm.uat.mobileguardian.com/profile/enroll?sig=DEP&is_dep=0&is_mdm_enroll=1&instance={code}',
	'safari_stage_enroll'  => 'https://qa-mdm.mobileguardian.com/profile/enroll?sig=DEP&is_dep=0&is_mdm_enroll=1&instance={code}',
	'dep_enroll'            => '',
	'configurator_enroll'   => ''

);

