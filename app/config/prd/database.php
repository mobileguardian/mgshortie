<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Database Connections
	|--------------------------------------------------------------------------
	|
	| Here are each of the database connections setup for your application.
	| Of course, examples of configuring each database platform that is
	| supported by Laravel is shown below to make development simple.
	|
	|
	| All database work in Laravel is done through the PHP PDO facilities
	| so make sure you have the driver for your particular database of
	| choice installed on your machine before you begin development.
	|
	*/
	'default' => 'mdm',
	'connections' => array(
		'mdm' => array(
			'driver'    => 'mysql',
			'host'      => 'mf-sql-master',
			'database' => 'mdm', //'mg-mdm-prd'
			'username' => 'mdm', //'mg-mdm-prd'
			'password' => 'MG8GcGjh21WtI', // 'VMPYk93p7H6jS5kHWYDB3B5'
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'options'   => array(
				PDO::ATTR_STRINGIFY_FETCHES => true
			),
		)
	),

);
