<?php

return array(
	'safari_prd_enroll'   => 'https://mdm.mobileguardian.com/profile/menroll?instance={code}',
	'safari_uat_enroll'    => 'https://mdm.uat.mobileguardian.com/profile/menroll?instance={code}',
	'safari_stg_enroll'  => 'https://mdm.stg.mobileguardian.com/profile/menroll?instance={code}',
    'dep_enroll'            => '',
    'configurator_enroll'   => ''

);

