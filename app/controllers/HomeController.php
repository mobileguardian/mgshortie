<?php
class HomeController extends BaseController {


	/*
	 * generate safari enroll URL
	 */
	public function index($code, $instance=null)
	{

		//check length and format
		if (strlen($code) != 9)
		{
			echo "Invalid Code";
			return;
		}

		$code = substr($code,0,1) . '-' . substr($code,1,4) . '-' . substr($code,5,4);
		switch($instance)
		{
			case 's':
				$safari_url = Config::get('application.safari_stg_enroll');
				break;
			case 'u':
				$safari_url = Config::get('application.safari_uat_enroll');
				break;
			case 'p':
			default:
				$safari_url = Config::get('application.safari_prd_enroll');
				break;
		}

		$safari_url = str_replace('{code}',$code,$safari_url);

		return Redirect::to($safari_url);
	}

}
